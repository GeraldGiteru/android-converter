package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button convertBtn, exitBtn;
    private EditText editMm, editInches;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editInches = (EditText) findViewById(R.id.editInches);
        editMm = (EditText) findViewById(R.id.editMm);

        convertBtn = (Button) findViewById(R.id.buttonConvert);

        convertBtn.setOnClickListener(new View.OnClickListener(){
                @Override
            public void onClick(View v){
                String getMM = editMm.getText().toString();
                    if(getMM.length() == 0 ){
                        Toast.makeText(getApplicationContext(),"Please first enter the amount of (mm) to convert",Toast.LENGTH_SHORT).show();
                    }else {
                        convertInches();
                    }

                }
                });

        exitBtn = (Button) findViewById(R.id.buttonExit);
        exitBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();
            }
        });
    }

    public void convertInches(){
        double mm = Double.parseDouble(editMm.getText().toString());
        double inches = mm/25.4;

        editInches.setText(String.format("%s",inches));
    }
}
